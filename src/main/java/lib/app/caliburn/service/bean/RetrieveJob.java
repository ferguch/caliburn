package lib.app.caliburn.service.bean;

import com.google.protobuf.InvalidProtocolBufferException;
import lib.app.caliburn.proto.JobProtos.JobRequestDto;
import lib.app.caliburn.service.exception.GetException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


@Component
public class RetrieveJob {

    public Object execute(final Long jobId) throws GetException {

        if (StringUtils.isEmpty(jobId)) {
            throw new GetException("Empty jobId received");
        }

        // TODO build key and retrieve JobRequestDto from KVS

        // Read job data
        JobRequestDto dto = JobRequestDto.newBuilder().setId(jobId.toString()).build();

        try {
            JobRequestDto jobRequestDto = JobRequestDto.parseFrom(dto.toByteArray());
            // TODO add any payloads to JobRequestDto
            return jobRequestDto;
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException("Failed to parse JobRequestDto", e);
        }
    }
}
