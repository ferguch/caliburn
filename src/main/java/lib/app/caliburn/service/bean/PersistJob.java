package lib.app.caliburn.service.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class PersistJob {

    private static final Logger log = LogManager.getLogger(PersistJob.class);

    private JmsTemplate jmsTemplate;

    @Value("${process.job-return.useBaton}")
    private Boolean useBaton;

    @Value("${queue.job-return.process.payload}")
    private String payloadQueue;

    @Value("${queue.job-return.process.baton}")
    private String batonQueue;

    @Autowired
    public PersistJob(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void execute(final byte[] jobResponse) {

        Long id = persist(jobResponse);

        if (useBaton) {
            log.info(String.format("Sending jobResponse [%s] to [%s] queue", id, batonQueue));
            jmsTemplate.convertAndSend(batonQueue, id);
        } else {
            log.info(String.format("Sending jobResponse [%s] to [%s] queue", id, payloadQueue));
            jmsTemplate.convertAndSend(payloadQueue, jobResponse);
        }
    }

    private Long persist(final byte[] jobResponse) {

        // TODO persist JobResponseDto and return id
        // Use different classes for different persistence mediums
        log.info(String.format("Persisting jobResponse [%s]", jobResponse));
        return new Random().nextLong();
    }
}
