package lib.app.caliburn.service.rest;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import lib.app.caliburn.proto.JobProtos.JobRequestDto;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/job")
public class RestJobService {

    // TODO handle error scenarios

    private static final Logger LOG = LogManager.getLogger(RestJobService.class);

    @Produce(uri = "seda:getJob")
    private ProducerTemplate getJobHandler;

    @Produce(uri = "seda:putJob")
    private ProducerTemplate putJobHandler;

    @GetMapping("/getJobAsJson/{id}")
    public @ResponseBody
    ResponseEntity<String> getJobAsJson(@PathVariable final Long id) {
        LOG.info(String.format("getJobAsJson [%s]", id));

        // Retrieve job request
        ResponseEntity response = getJob(id);

        try {
            return new ResponseEntity<>(JsonFormat.printer().print((JobRequestDto) response.getBody()), HttpStatus.OK);
        } catch (InvalidProtocolBufferException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getJob/{id}")
    public @ResponseBody
    ResponseEntity<JobRequestDto> getJob(@PathVariable final Long id) {
        LOG.info(String.format("getJob [%s]", id));

        // Retrieve job request
        JobRequestDto jobRequestDto = getJobHandler.requestBody(id, JobRequestDto.class);

        return new ResponseEntity<>(jobRequestDto, HttpStatus.OK);
    }

    @PutMapping("/putJob")
    public @ResponseBody
    ResponseEntity<String> putJob(@RequestBody byte[] jobResponse) {

        LOG.info(String.format("putJob [%s]", jobResponse));

        // Persist job response
        String jobResponseId = putJobHandler.requestBody(jobResponse, String.class);

        return new ResponseEntity<>(jobResponseId, HttpStatus.OK);
    }
}