package lib.app.caliburn.service.routes;

import lib.app.caliburn.service.bean.PersistJob;
import lib.app.caliburn.service.exception.PutException;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UpdateJobRoute extends RouteBuilder {

    private static final String ROUTE_NAME = "UpdateJobRoute";

    @Value("${dir.job-return.ingest}")
    private String ingestDir;

    @Value("${queue.job-return.ingest}")
    private String ingestQueue;

    @Value("${queue.job-return.error}")
    private String errorQueue;

    @Override
    public void configure() {

        onException(PutException.class)
                .handled(true)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404))
                .log(LoggingLevel.ERROR, "PutException: Error processing JobResponse, not found")
                .to("jms:queue:" + errorQueue);

        onException(Exception.class)
                .handled(true)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
                .log(LoggingLevel.ERROR, "Exception: Error processing JobResponse")
                .to("jms:queue:" + errorQueue);

        // Retry files
        from("file:" + ingestDir)
                .description("Process job response file from directory + " + ingestDir)
                .convertBodyTo(byte[].class, "iso-8859-1")
                .to("jms:queue:" + ingestQueue);

        from("jms:queue:" + ingestQueue)
                .description("Process job response from queue + " + ingestQueue)
                .convertBodyTo(byte[].class, "iso-8859-1")
                .to("seda:putJob");

        // Update job
        from("seda:putJob")
                .routeId(ROUTE_NAME)
                .log(LoggingLevel.INFO, String.format("Put Job [%s]", "${body}"))
                .bean(PersistJob.class);

    }
}
