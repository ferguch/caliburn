package lib.app.caliburn.service.routes;

import lib.app.caliburn.service.bean.RetrieveJob;
import lib.app.caliburn.service.exception.GetException;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetJobRoute extends RouteBuilder {

    public static final String ROUTE_NAME = "GetJobRoute";

    @Autowired
    private RetrieveJob readJob;

    @Override
    public void configure() {

        onException(GetException.class)
                .handled(true)
                //.redeliveryPolicy()
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404))
                .setHeader(Exchange.CONTENT_TYPE, constant("text/plain"));
        onException(Exception.class)
                .handled(true)
                //.redeliveryPolicy()
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
                .setHeader(Exchange.CONTENT_TYPE, constant("text/plain"));
        //.setBody().constant();

        // Read job data
        from("seda:getJob")
                .routeId(ROUTE_NAME)
                .log(LoggingLevel.INFO, String.format("Get Job [%s]", "${body}"))
                .bean(RetrieveJob.class);
    }
}
