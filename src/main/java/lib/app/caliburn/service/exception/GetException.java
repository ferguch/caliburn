package lib.app.caliburn.service.exception;

public class GetException extends Exception {

    public GetException(final String message) {
        super(message);
    }
}
