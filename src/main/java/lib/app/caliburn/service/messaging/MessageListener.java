package lib.app.caliburn.service.messaging;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * This is the queue messaging class, its receiveMessage() method is invoked with the
 * message as the parameter.
 */
@Component
public class MessageListener {

    private static final Logger log = LogManager.getLogger(MessageListener.class);

//    @JmsListener(destination = Application.JOB_RETURN_QUEUE)
//    public void receiveMessage(Map<String, String> message) {
//        // TODO process message from queue
//        log.info("Received <" + message + ">");
//        Long id = Long.valueOf(message.get("id"));
//
//        log.info("Message processed...");
//    }
}
