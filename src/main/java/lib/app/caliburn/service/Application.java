package lib.app.caliburn.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
@EnableAutoConfiguration
@EnableJms
@ComponentScan(basePackages = "lib.app.caliburn")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

