Application RESTful Interface
-----------------------------
Spring Boot app to provide REST interface to process Job data

GET /caliburn/job/<id>

PUT /caliburn/job/<data>

Maven Build and Deploy
----------------------------

To build a docker image for the app:
* mvn docker:build

To run the docker container:
* mvn docker:run

Docker Repo and Running App
---------------------------

Repo location:
https://cloud.docker.com/repository/docker/ferguch/caliburn

To push the docker image to dockerhub:
* docker push ferguch/caliburn:tagname

To run docker container:
* docker run -p 8010:8010 ferguch/caliburn

Run ActiveMQ broker container:
* docker run -d -e 'ACTIVEMQ_CONFIG_NAME=amqp-srv1' -e 'ACTIVEMQ_CONFIG_DEFAULTACCOUNT=false' -e 'ACTIVEMQ_ADMIN_LOGIN=admin' -e 'ACTIVEMQ_ADMIN_PASSWORD=password' -e 'ACTIVEMQ_CONFIG_QUEUES_example=examplequeue' -p 8161:8161 -p 61616:61616 -p 61613:61613 webcenter/activemq

